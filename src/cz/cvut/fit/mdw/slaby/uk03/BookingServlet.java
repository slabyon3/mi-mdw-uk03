package cz.cvut.fit.mdw.slaby.uk03;

import weblogic.servlet.annotation.WLServlet;

import javax.servlet.ServletException;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;
import javax.servlet.http.HttpSession;
import java.io.IOException;
import java.io.PrintWriter;

@WLServlet(name = "Booking Servlet", mapping = "/book")
public class BookingServlet extends javax.servlet.http.HttpServlet {
    private static final String STATE_ATTRIBUTE = "STATE_ATTRIBUTE";
    private static final String INTRO_MESSAGE = "Welcome to the trip booking service!\n" +
            "Please specify your trip ID and registration name in the fields 'tid' and 'name' respectively.";
    private static final String NEW_MESSAGE = "Thank you!\n" +
            "You have entered trip ID %s under the following name: %s.\n" +
            "Please send your payment to our bank account.";
    private static final String PAYMENT_MESSAGE = "Thank you for your payment!\n" +
            "You have completed your booking for trip ID %s. Have a nice day, %s!";
    private static final String ERROR_MESSAGE = "The parameters you sent are invalid!\n" +
            "Please try again.";
    private static final String NAME_ATTRIBUTE = "name";
    private static final String TRIPID_ATTRIBUTE = "tid";

    protected void doPost(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        PrintWriter writer = response.getWriter();
        HttpSession session = request.getSession(true);

        if (session.getCreationTime() == session.getLastAccessedTime()) {
            session.setAttribute(STATE_ATTRIBUTE, State.NEW);
            writer.println(INTRO_MESSAGE);
            return;
        }

        State state = (State) session.getAttribute(STATE_ATTRIBUTE);

        switch (state) {
            case NEW:
                String name = request.getParameter(NAME_ATTRIBUTE);
                String tripid = request.getParameter(TRIPID_ATTRIBUTE);
                if (!checkParameters(name, tripid, writer))
                    return;
                session.setAttribute(NAME_ATTRIBUTE, name);
                session.setAttribute(TRIPID_ATTRIBUTE, tripid);
                writer.println(String.format(NEW_MESSAGE, tripid, name));
                break;
            case PAYMENT:
                writer.println(String.format(PAYMENT_MESSAGE,
                        session.getAttribute(TRIPID_ATTRIBUTE),
                        session.getAttribute(NAME_ATTRIBUTE)));
                session.invalidate();
                return;
            default:
                break;
        }

        session.setAttribute(STATE_ATTRIBUTE, state.nextState());
    }

    private boolean checkParameters(String name, String tripid, PrintWriter writer) {
        if (name == null || name.isEmpty() || tripid == null || tripid.isEmpty()) {
            writer.println(ERROR_MESSAGE);
            return false;
        }
        return true;
    }

    protected void doGet(HttpServletRequest request, HttpServletResponse response)
            throws ServletException, IOException {
        doPost(request, response);
    }

    public enum State {
        NEW,
        PAYMENT;

        public State nextState() {
            switch (this) {
                case NEW:
                    return PAYMENT;
                case PAYMENT:
                default:
                    return this;
            }
        }
    }
}
